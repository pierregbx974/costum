

/*[] Enregistre citoyens Ou tu regardes si il existe déjà mettre à jour ces infos 

[] Recherche si groupe existe par adress 
	[] si oui retour avec lemail framalist
	[] si non création du groupe
		[] nom automatique / code automatique / email framaliste auto
		[] envoie mail au pacte nouveau citoyen gérard - email framaliste
[] Retour data
	bool exist (pour le groupe) 
	object elt (frama) 
[] Retour subscribe auto mailing sendInblue
[] Mot si cest le premier à créer 
[] Mot avec email framaliste suscribe auto à la mailing*/
var pacte={
	initScopeObj : function(){
		$(".content-input-scope-pacte").html(scopeObj.getHtml("Code postal"));
		var params = {
			subParams : {
				cities : {
					type : ["cities"],
					country : ["FR", "RE", "GP", "GF", "MQ", "YT", "NC", "PM"],
					cp : true
				}
			}
		}
		scopeObj.initVar(params);
		scopeObj.init();
	},
	mapDefault : function(){
		mapCustom.popup.default = function(data){
			mylog.log("mapCO mapCustom.popup.default", data);
			var id = (typeof data.id != "undefined") ? data.id :  data._id.$id ;

			var imgProfil = mapCustom.custom.getThumbProfil(data) ;

			var popup = "";
			popup += "<div class='padding-5' id='popup"+id+"'>";
				popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
				popup += "<span style='margin-left : 5px; font-size:18px'>" + data.name + "</span>";
				
				if (typeof data.email != "undefined" && data.email != null ){
					popup += "<div id='pop-contacts' class='popup-section'>";
						popup += "<div class='popup-subtitle'>Contact</div>";
							popup += "<div class='popup-info-profil'>";
								popup += "<i class='fa fa-envelope fa_email'></i> <a href='mailto:"+data.email+"'>" + data.email+"</a>";
							popup += "</div>";
						popup += "</div>";
					popup += "</div>";
				}
				popup += "<div class='popup-section'>";
					popup += "<a href='javascript:;' onclick='pacte.joinGroupMap(\""+id+"\");' target='_blank' class='item_map_list popup-marker' id='popup"+id+"'>";
						popup += '<div class="btn btn-sm btn-more col-xs-12">';
						popup += 	"Rejoindre le collectif"
					popup += '</div></a>';
				popup += '</div>';
			popup += '</div>';
			return popup;
		};
	},
	joinGroupMap : function(id){
		groupSelected=mapPacteHome.data[id];
		scopeObj.selected=groupSelected.scope;
		$.each(groupSelected.scope, function(e,v){
			zoneName= (typeof v.cityName != "undefined") ? v.cityName : v.name ;
		});
		pacte.launchRegister({exist : true}, zoneName);
	},
	launchRegister : function(result, city){
	    $(".form-register .msgGroup").remove();
	    $('#modalRegister').modal("show");
	    var params={};
	    if(result.exist){
	    	params.elt=elt;
  			params.msgHeader='<span class="text-justify">Bienvenue ! Plusieurs habitant.es de <b>'+city+'</b> se sont déjà regroupé.es pour encourager la transition.<br/>'+ 
						'Remplissez ce formulaire pour entrer en contact avec eux !'+								
					'</span>';
			$(".form-register .emailRegister").after("<div class='msgGroup'>"+
				"<label class='letter-black'><i class='fa fa-pencil'></i> Ecrire un message au groupe</label>"+
				"<textarea class='form-control' id='textMsgGroup' name='textMsgGroup' placeholder='Ecrire un message'></textarea>"+
    		"</div>");
  		}else{
  			params.elt=elt;
  			params.msgHeader='<span class="text-justify">Vous êtes le.la premier.e à souhaiter encourager la transition de <b>'+city+'</b> en portant le Pacte !<br/>'+
					'Remplissez ce formulaire, nous vous mettrons en contact avec les prochaines personnes de votre commune qui s’inscriront !<br/> Personne ne peut impulser le changement seul, n’hésitez pas à en parler autour de vous, à vos amis, vos voisins, ou sur <a href="https://www.facebook.com/PacteTransition" target="_blank" class="text-orange">Facebook</a> et <a href="https://twitter.com/PacteTransition" target="_blank" class="text-orange">Twitter</a> !'+
				'</span>';
  		}
  		if($(".form-register .info-register-form").length > 0)
  			$('.form-register .info-register-form').html(params.msgHeader);
  		else
			$('.form-register').find(".surnameRegister").before("<div class='col-xs-12 bg-purple text-white text-center info-register-form'>"+params.msgHeader+"</div>");
		Login.runRegisterValidator();
	}
};
scopeObj.onclickScope = function () {
	//if($(".content-input-scope-pacte .item-globalscope-checker[data-scope-value='54c09656f6b95c1418008e00cities']").length > 0)
	$(".item-globalscope-checker[data-scope-value='54c09656f6b95c1418008e00cities'], .item-globalscope-checker[data-scope-value='54c09653f6b95c141800849ecities'], .item-globalscope-checker[data-scope-value='54c09653f6b95c141800849ecities'], .item-globalscope-checker[data-scope-value='54c09638f6b95c141800266ccities']").remove();
	$(".content-input-scope-pacte .item-globalscope-checker").off().on('click', function(){
		var key = $(this).data("scope-value");
		if( typeof myScopes != "undefined" &&
				typeof myScopes.search != "undefined" &&
				typeof myScopes.search[key]  != "undefined" ){
					var scopeDF = myScopes.search[key];
					var nameZone = (typeof scopeDF.cityName != "undefined") ? scopeDF.cityName : scopeDF.name ;
		}
		scopeObj.selected={};
		scopeObj.selected[key] = myScopes.search[key];
		$.ajax({
	    	  	type: "POST",
	    	  	url: baseUrl+"/costum/pacte/checkexist",
	    	  	data: {
	    	  		scope:scopeObj.selected,
	    	  	},
	    	 	success: function(data){
	    	 		pacte.launchRegister(data, nameZone);
	    		}
		});
				
			
	});
};
Login.runRegisterValidator = function(params) { 
	console.log("runRegisterValidatorCOOOOOOOOOOOOOOOOOOOOOOOOOOSTUME!!!!");
	//alert("runRegisterValidator 3");
	var form4 = $('.form-register');
	var errorHandler3 = $('.errorHandler', form4);
	var createBtn = null;
	if($(".form-register .surnameRegister").length <= 0){
	$('.form-register').find(".nameRegister").before('<div class="surnameRegister">'+
                    '<label class="letter-black"><i class="fa fa-address-book-o"></i> Prénom</label>'+
                    '<input class="form-control" id="registerSurname" name="surnname" type="text" placeholder="Prénom"><br/>'+
               '</div>');
	}
	$('.form-register').find(".nameRegister label").html('<i class="fa fa-address-book-o"></i> Nom');
	$('.form-register').find(".nameRegister input").attr("placeholder","Nom");
	$('.form-register').find(".usernameRegister").remove();
	$('.form-register').find(".passwordRegister").remove();
	$('.form-register').find(".passwordAgainRegister").remove();
	$('.form-register').find(".agreeMsg").removeClass("text-red").addClass("text-purple").html("Je m’engage à respecter la <a href='https://nextcloud.transition-citoyenne.org/index.php/s/7ywrCwNPF8TckPN' target='_blank' class='text-orange'>charte des collectifs locaux</a>");
	/*if($(".form-register .newsletterContent").length <= 0){
		checkNewsletter='<div class="form-group pull-left newsletterContent margin-top-10" style="width:100%;">'+
                    	'<div class="checkbox-content pull-left no-padding">'+
                        	'<label for="newsletter" class="">'+
                            	'<input type="checkbox" class="newsletter checkbox-info" id="newsletter" name="newsletter">'+
                            	'<span class="cr"><i class="cr-icon fa fa-check"></i></span>'+
                        	'</label>'+
                        '</div>'+
                        '<span class="checkbox-msg">Je souhaite recevoir la newsletter du Pacte</span>'+
                    '</div>';
		$('.form-register').find(".agreeContent").after(checkNewsletter);
	}*/
	// console.log("runRegisterValidatorCOOOOOOOOOOOOOOOOOOOOOOOOOOSTUME!!!! form4", form4.valid() );
	// if(form4.valid())
	// 	form4.destroy();
	form4.validate({
		rules : {
			name : {
				required : true,
				minlength : 2
			},
			email3 : {
				required : { 
				 	depends:function(){
				 		$(this).val($.trim($(this).val()));
				 		return true;
				 	}
				},
				email : true
			}//,
			//agree: {
			//	agreeValidation : true
				/*required:true,
				minlength: 1,
				depends: function() {
					alert();
					return false;
				}*/
				//required : true,
				//checkbox:["element", "elelme"]
				/*minlength : 1,*/
				/*required : function(el){
					//depends:function(){
								alert();
					 			if($(this).is(":checked")){
					 				alert();
					 				return true;
					 				
					 			}else{
					 				alert("false");
					 			}
					 	//	}
					 	}*/
			//}
		},

		//messages: {
		//	agree: trad["mustacceptCGU"],
		//},
		submitHandler : function(form) { 
			//alert("runRegisterValidator 4");
			console.log("runRegisterValidator submitHandler COSTUM", form);
			if(!form4.find("#agree").is(":checked")){
				var validator = form4.validate();
					validator.showErrors({
  						"agree": "Vous devez vous engager à respecter la charte des collectifs locaux avant de vous inscrire !"
				});
				return false;
			}
			
			errorHandler3.hide();
			$(".createBtn").prop('disabled', true);
    		$(".createBtn").find(".fa").removeClass("fa-sign-in").addClass("fa-spinner fa-spin");
			var params = { 
			   "name" : $('.form-register #registerSurname').val()+" "+$('.form-register #registerName').val(),
			   "email" : $(".form-register #email3").val(),
			   "pendingUserId" : pendingUserId
            };
            if($('.form-register #isInvitation').val())
            	params.isInvitation=true;
            if(Object.keys(scopeObj.selected).length > 0){
		  		params.scope = scopeObj.selected;
		  	}
		  	if($(".form-register #newsletter").is(":checked"))
		  		params.newsletter=true;
		  	if($('.form-register .msgGroup').length && $(".form-register #textMsgGroup").val() != "")
		  		params.msgGroup=$(".form-register #textMsgGroup").val();
		  	$.ajax({
	    	  type: "POST",
	    	  url: baseUrl+"/costum/pacte/register",
	    	  data: params,
	    	  success: function(data){
	    		  if(data.result) {
	    		  	//createBtn.stop();
					$(".createBtn").prop('disabled', false);
    				$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
					$("#registerName").val("");
					$("#username").val("");
					$("#email3").val("");
					$("#password3").val("");
					$("#passwordAgain").val("");
					$("#passwordAgain").val("");
					$("#registerSurname").val("");
					$('#agree').prop('checked', false);
					$('#newsletter').prop('checked', false);
				    toastr.success("Merci, votre inscription a bien été prise en compte. Vous allez être mis en contact par mail avec votre collectif local");
    		  		$('.modal').modal('hide');
    		  		scopeObj.selected={};
	    		  }
	    		  else {
	    		  	toastr.error(data.msg);
	    		  	$('.modal').modal('hide');	    		  	
    		  		scopeObj.selected={};
	    		  }
	    	  },
	    	  error: function(data) {
	    	  	toastr.error(trad["somethingwentwrong"]);
	    	  	$(".createBtn").prop('disabled', false);
    			$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
	    	  	//createBtn.stop();
	    	  },
	    	  dataType: "json"
	    	});
		    return false;
		},
		invalidHandler : function(event, validator) {//display error alert on form submit
			errorHandler3.show();
			$(".createBtn").prop('disabled', false);
    		$(".createBtn").find(".fa").removeClass("fa-spinner fa-spin").addClass("fa-sign-in");
			//createBtn.stop();
		}
	});
};
//jQuery(document).ready(function() {
Login.runRegisterValidator();
//});