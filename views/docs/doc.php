<?php 
/* TODO 

[ ] reload is empty on hope page
[ ] reload on any page 
[ ] use categories or types to organize poi.docs 
[ ] create poi.timeline as an action plan 
[ ] edit poi.doc
[ ] delete poi.doc

wishlist 
[ ] poi.doc templates
[ ] move doc position index
[ ] use poi.html as piece of a page

*/

?>

<div id="header-doc" class="shadow2">
	<a href='javascript:;' id="show-menu-xs" class="visible-xs visible-sm pull-left" data-placement="bottom" data-title="Menu"><i class="fa fa-bars"></i></a>
	<h2 class="elipsis no-margin"><i class="fa fa-book hidden-xs"></i> <?php echo Yii::t("docs", "All <span class='hidden-xs'>you need to know</span> about") ?> LE <span style="color:#65BA91">CONTRAT DE TRANSITION ÉCOLOGIQUE</span></h2>
	
    <a href='javascript:;' class="lbh pull-right" id="close-docs"><span><i class="fa fa-sign-out"></i> <?php echo Yii::t("common","Back") ?></span></a>
</div>

<div id="menu-left" class="col-md-3 col-sm-2 col-xs-12 shadow2">
  	<ul class="col-md-12 col-sm-12 col-xs-12 no-padding">
		
		<li class="col-xs-12 no-padding">
			<a href="javascript:;" class="link-docs-menu down-menu" data-type="cte" data-dir="costum.views.custom.ctenat.docs">
				<i class="fa fa-angle-right"></i> Parcours
			</a>
			<ul class="subMenu col-xs-12 no-padding">
				<li class="col-xs-12 no-padding">
					<a href="javascript:;"  id="startDoc"  class="link-docs-menu" data-type="public" data-dir="costum.views.custom.ctenat.docs">
						Acteurs publics
					</a>
				</li>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="socioeco" data-dir="costum.views.custom.ctenat.docs">
						Acteurs socio-économiques
					</a>
				</li>
				
			</ul>
		</li>
			
			
		<?php 

		$pois = PHDB::find( Poi::COLLECTION, array( "source.key" => "ctenat", "type"=>"doc") );
		$structField = "structags";
		$pois = Poi::hierarchy($pois,$structField);
	
		foreach ($pois["parentTree"] as $key => $value) 
		{ ?>
			<li class="col-xs-12 no-padding">
				<a href="javascript:" class="link-docs-menu-poi" data-poi="<?php echo (string)$pois["orphans"][$key]["_id"]; ?>" >
					<i class="fa fa-angle-right"></i> <?php echo $pois["orphans"][$key]["name"]; ?>
				</a>
				<ul class="subMenu col-xs-12 no-padding">
					<?php
						foreach ( $value as $k => $v) { ?>
						<li class="col-xs-12 no-padding">
							<a href="javascript:" class="link-docs-menu-poi" data-poi="<?php echo (string)$v["_id"]; ?>" >
								<?php echo $v["name"]; ?>
							</a>
						</li>
					<?php
						}
					?>
				</ul>
			</li>
		<?php
		}
		
		foreach ($pois["orphans"] as $key => $value) { 
			if( isset($value[$structField]) && count($value[$structField]) == 1 && isset( $pois["parentTree"][ $value[$structField][0] ]) ){
				//on n'affiche pas le sparent en tant qu'orphelin
			}
			else if( isset($value[$structField]) && isset( $pois["parentTree"][ $value[$structField] ]) ){
				//on n'affiche pas le sparent en tant qu'orphelin
			}
			else
			{ ?>
			<li class="col-xs-12 no-padding">
				<a href="javascript:" class="link-docs-menu-poi" data-poi="<?php echo (string)$value["_id"]; ?>" >
					<?php echo $value["name"]; ?>
				</a>
			</li>
		<?php } } ?>

		
		<li class="col-xs-12 no-padding">	
			<ul>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="createDocBtn text-red"	>
						<i class="fa fa-plus-circle"></i> AJOUTER
					</a>
				</li>
			</ul>
		</li>
		
	</ul>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		mylog.log("render","costum.views.ctenat.doc");
		
		
		setTimeout(function() { navInDocs("cte", "costum.views.custom.ctenat.docs"); }, 500)
		
		$("#close-docs").attr("href",urlBackHistory);

		$(".link-docs-menu-poi").off().on("click",function(){
			//alert(".link-docs-menu-poi");
			var url = baseUrl+'/co2/element/get/type/poi/id/'+$(this).data("poi")+"/edit/true";
			ajaxPost(null ,url, null,function(data){
				descHtml = dataHelper.markdownToHtml(data.map.description); 
				descHtml += data.map.editBtn;
	   			$('#container-docs').html(descHtml);
	   			$(".editThisBtn").off().on("click",function (){
					mylog.log("editThisBtn");
					var id = $(this).data("id");
				    var type = $(this).data("type");
					dyFObj.editElement(type,id,null,dynFormCostumDoc)
				});
			},"html");

		});

		var dynFormCostumDoc = {
			"beforeBuild":{
			        "properties" : {
			            "structags" : {
			                "inputType" : "tags",
			                "placeholder" : "Structurer le contenu",
			                "values" : null,
			                "label" : "Structure et Hierarchie (parent ou parent.enfant)"
			            }
			        }
			    },
		    "onload" : {
		        "actions" : {
		            "setTitle" : "Documentation",
		            "html" : {
		                "nametext>label" : "Titre de la documentation",
		                "infocustom" : "<br/>Créer des section et des sous chapitres"
		            },
		            "presetValue" : {
		                "type" : "doc"
		            },
		            "hide" : {
		                "locationlocation" : 1,
		                "breadcrumbcustom" : 1,
		                "urlsarray" : 1,
		                "imageuploader":1,
		                "tagstags":1
		            }
		        }
		    }
		};
		
		
		
		$(".createDocBtn").off().on("click",function (){
			mylog.log("createBtn");
			dyFObj.openForm('poi',null,null,null,dynFormCostumDoc)
		});
	})
</script>

<!-- 
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="map" data-dir="costum.views.custom.ctenat.docs">
						CARTE 
					</a>
				</li>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="calendrier" data-dir="costum.views.custom.ctenat.docs">
						CALENDRIER 
					</a>
				</li>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="ministere" data-dir="costum.views.custom.ctenat.docs">
						MINISTERE DE L'EGOLOGIE
					</a>
				</li>
				<li class="col-xs-12 no-padding">
					<a href="javascript:;" class="link-docs-menu" data-type="pcaet" data-dir="costum.views.custom.ctenat.docs">
						PCAET from views folder
					</a>
				</li>

				<li class="col-xs-12 no-padding">
					<a href="javascript:" class="link-docs-menu down-menu" data-type="welcome" data-dir="costum.views.custom.ctenat.docs">
						<i class="fa fa-angle-right"></i> COMPRENDRE
					</a>
				</li>

				<li class="col-xs-12 no-padding">
					<a href="javascript:" class="link-docs-menu down-menu" data-type="participer" data-dir="costum.views.custom.ctenat.docs">
						<i class="fa fa-angle-right"></i> PARTICIPER
					</a>
				</li> -->