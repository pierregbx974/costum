<?php 
$defaultColor = "#354C57"; 
$structField = "structags";
?>
<div id="wizard" class="swMain">

    <style type="text/css">
        .swMain ul li > a.done .stepNumber {
            border-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;
            background-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>; 
        }

        swMain > ul li > a.selected .stepDesc, .swMain li > a.done .stepDesc {
         color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;  
         font-weight: bolder; 
        }

        .swMain > ul li > a.selected::before, .swMain li > a.done::before{
          border-color: <?php echo ( @$color1 ) ? $color1 : $defaultColor ?>;      
        }
    </style>


    <ul id="wizardLinks">
        
        <?php
        //var_dump($poiList); 
        foreach ($listSteps as $k => $v) {
            $n = "todo";
            $p = null;
            
            if( count(Poi::getPoiByStruct($poiList, "step".$v,$structField ) ) != 0 ) { 
                $p = Poi::getPoiByStruct($poiList, "step".$v,$structField )[0];
                $n =  $p["name"];
            }
            echo "<li>";

                $d = ( isset($p) && !empty( $p[ "description" ] ) ) ? 'class="done"' : '';
                $l = ( 
                    (!isset($p) && Authorisation::canEditItem(Yii::app()->session["userId"], Yii::app()->session["costum"]["contextType"], Yii::app()->session["costum"]["contextId"])) || 
                    ( isset($p) && !empty( $p[ "description" ] )  )  ) ? 'showStep(\'#'.$v.'\')' : '';
                $lbl = ( isset($p) && !empty( $p[ "description" ] ) ) ? $k : "?";
                
                echo '<a onclick="'.$l.'" href="javascript:;" '.$d.' >';
                echo '<div class="stepNumber">'.$lbl.'</div>';
                echo '<span class="stepDesc">'.$n.'</span></a>';
            echo "</li>";    
        }
        ?>

    </ul>
    

    <?php  
    foreach ($listSteps as $k => $v) {
        $hide = ($k==0) ? "" : "hide";
    ?>
    <div id='<?php echo $v ?>' class='col-sm-offset-1 col-sm-10 sectionStep <?php echo $hide ?>' style="padding-bottom:40px">
        <?php 
        if( count(Poi::getPoiByStruct($poiList,"step".$v,$structField))!=0 )
        {   
            $p = Poi::getPoiByStruct($poiList,"step".$v,$structField)[0];
            echo '<h1 class="text-red">'.@$p["name"].'</h1>';
            echo "<div class='markdown'>".@$p["description"]."</div>";
            
            if( isset($p["documents"]) ){
              echo "<br/><h4>Documents</h4>";
              //var_dump($p["documents"]);
              foreach ($p["documents"] as $key => $doc) {
                $dicon = "fa-file";
                $fileType = explode(".", $doc["name"])[1]; 
                if( $fileType == "png" || $fileType == "jpg" || $fileType == "jpeg" || $fileType == "gif" )
                  $dicon = "fa-file-image-o";
                else if( $fileType == "pdf" )
                  $dicon = "fa-file-pdf-o";
                else if( $fileType == "xls" || $fileType == "xlsx" || $fileType == "csv" )
                  $dicon = "fa-file-excel-o";
                else if( $fileType == "doc" || $fileType == "docx" || $fileType == "odt" || $fileType == "ods" || $fileType == "odp" )
                  $dicon = "fa-file-text-o";
                else if( $fileType == "ppt" || $fileType == "pptx" )
                  $dicon = "fa-file-text-o";
                echo "<a href='".$doc["path"]."' target='_blanck'><i class='text-red fa ".$dicon."'></i> ".$doc["name"]."</a><br/>";
              }
            }
            $edit ="update";
        } 
        else 
        { ?>
        TEXT TODO <br/>
        as POI type cms + tag : step<?php echo $v ?>
        <?php  
        $edit ="create";
        }

        echo $this->renderPartial("costum.views.tpls.openFormBtn",
                                array(
                                    'edit' => $edit,
                                    'tag' => 'step'.$v,
                                    'id' => (string)@$p["_id"]
                                 ),true);
        
        

        ?>


        
        
    </div>
    <?php  
    }  

    echo $this->renderPartial("costum.views.tpls.dynFormCostumCMS"); 
 ?>

    <script type="text/javascript">
jQuery(document).ready(function() {
    mylog.log("render","costum.views.tpls.wizard");
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });
});
        function showStep(id){
            $(".sectionStep").addClass("hide");
            $(id).removeClass("hide");
        }

        //OPTIM : ce code est répété autant de fois qu'il y a de btn 
    //il devrait etre sur l'appelant mais du il sera répété un peu partout 
    
    
    
    $(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dyFObj.editElement(type,id,null,dynFormCostumCMS)
    });
    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBtn");
        dyFObj.openForm('poi',null,{structags:$(this).data("tag") ,type:'cms'},null,dynFormCostumCMS)
    });

    $(".deleteThisBtn").off().on("click",function (){
        mylog.log("deleteThisBtn click");
          $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
          var btnClick = $(this);
          var id = $(this).data("id");
          var type = $(this).data("type");
          var urlToSend = baseUrl+"/co2/element/delete/type/"+type+"/id/"+id;
          
          bootbox.confirm(trad.areyousuretodelete,
            function(result) 
            {
                if (!result) {
                  btnClick.empty().html('<i class="fa fa-trash"></i>');
                  return;
                } else {
                  $.ajax({
                        type: "POST",
                        url: urlToSend,
                        dataType : "json"
                    })
                    .done(function (data) {
                        if ( data && data.result ) {
                          toastr.info("élément effacé");
                          $("#"+type+id).remove();
                        } else {
                           toastr.error("something went wrong!! please try again.");
                        }
                    });
                }
            });

    });
    </script>


</div>