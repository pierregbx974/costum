<?php
class RegisterAction extends CAction
{
	public function run(){ 	
		
		$res = array(
			"result" => false,
			"msg"  => ""
		);
		
		if(!empty($_POST["scope"]) && !empty($_POST["email"])){
			$html = "";
			//Check if user exist
			// - Create account with no mail sended
			$userMail=$_POST["email"];
			$user = PHDB::findOne(Person::COLLECTION, array( "email" => $_POST["email"] ), array("_id", "name", "email"));
			if(empty($user)){
				$newPerson = array( "name" => $_POST["name"],
									"email" => $_POST["email"],
									"invitedBy" => "");
				$user = Person::createAndInvite($newPerson, "");
				$html .= "<span>Un nouvelle utilisateur s'est connecté : ".$_POST["name"]."</span><br/>";
			}else{
				$user["id"] = (String) $user["_id"];
				$html .= "<span>Un utilisateur s'est connecté : ".$user["name"]."</span><br/>";
			}

			// boolean to send mail notification to pacte in order to validate the group for email frama process
			$sendTopacteCreate=false;
			
			/***** Begin - process to know which locality is concerned *****
			* - Get the name and the postal code selected by the user
			* - Cities with multi postal code (ex : Lille , Rennes , etc) will return the first entry considering as main entry by Pacte's collectif
			***********************************************************/
			foreach ($_POST["scope"] as $key => $value) {
				$name = (isset($value["cityName"]) && !empty($value["cityName"])) ? ucfirst(strtolower($value["cityName"])) : $value["name"];
				$checkScope=$key;
				$postalCode=$value["postalCode"];
				$city = City::getById($value["city"], array("postalCodes", "geo", "geoPosition"));
				if(count($city["postalCodes"]) > 1){
					$postalCode=$city["postalCodes"][0]["postalCode"];
					$name = (isset($city["postalCodes"][0]["name"]) && !empty($city["postalCodes"][0]["name"])) ? ucfirst(strtolower($city["postalCodes"][0]["name"])) : $name;
					$checkScope=(string)$city["_id"]."cities".$postalCode;
				}
			}
			// END PREPARING LOCALITY PROCESS

			// CHECK IF COLLECTIF IS ALREADY CREATED
			$name = "Collectif local ".trim($name);
			$where = array( "source.key" => Yii::app()->session["costum"]["slug"],
							"scope.".$checkScope => array('$exists' => true )  );
			$exist = PHDB::findOne(Organization::COLLECTION, $where, array("_id", "name", "email"));

			if(!empty($exist)){
				$res["exist"] = true;
				$res["elt"] = $exist;
				$res["email"] = $exist["email"];
				//On rentre dans le cas ou l'orga existe et la mailing est créée 
				if(!isset($exist["source.toBeValidated"]) || empty($exist["source.toBeValidated"])){
					// TODO MAIL TO FRAMA
					//Si message groupe on envoie le mail avec le texte de l'utilisateur
					$sub=explode("@", $exist["email"])[0];
					$paramsMails = array("tplMail" => $sub."-subscribe@listes.transition-citoyenne.org",
									"tplObject" => "sub ".$sub,
									"tpl" => "basic",
									"fromMail"=>$userMail,
									"html" => $html);
					Mail::createAndSend($paramsMails);
					if(isset($_POST["msgGroup"])){
						$tplMsgList='<span><b>'.$_POST["name"].'</b> a écrit ce message :</span><br/><span style="padding:15px; margin-top:10px;background-color:#f9f9f9; border:1px solid #eee; border-radius: 10px;float:left;">'.$_POST["msgGroup"].'</span>';
						$paramsMails = array("tplMail" => $exist["email"],
									"tplObject" => "Message d'un nouveau membre sur le ".$sub,
									"tpl" => "basic",
									"fromMail"=>$userMail,
									"html" => $tplMsgList);
						Mail::createAndSend($paramsMails);
					}
				}else
					$sendTopacteCreate=true;
			}else{
				$res["exist"] = false;

				$res["email"] = "pacte-".mb_strtolower($postalCode)."@listes.transition-citoyenne.org";
				$orga = array(	"name" => $name,
								"collection" => Organization::COLLECTION, 
								"type" => Organization::TYPE_GROUP,
								"email" => $res["email"],
								"creator" => $user["id"],
								"scope" => $_POST["scope"],
								"geo"=>$city["geo"],
								"geoPosition"=>$city["geoPosition"],
								"preferences"=>array("private"=>true,"isOpenData"=>true, "isOpenEdition"=>false),
								"created" => time(),
								"source" => array(
									"key" => Yii::app()->session["costum"]["slug"],
									"keys" => array(Yii::app()->session["costum"]["slug"]),
									"origin" => "costum")
								);
				/**** Begin - process to know if a group exist with the postal code *****
				* - Case of cities who shared the same cp (62223 => Anzin-st-aubin / Roclincourt / ecurie / )
				* - Will create the group to see it on the map 
				* - Collectif will be linked with the same email to mutualize 
				************************************************************************/
				$where = array( "source.key" => Yii::app()->session["costum"]["slug"],
							"email"=> $orga["email"] );
				$findSameEmail = PHDB::findOne(Organization::COLLECTION, $where, array("_id", "name", "email", "source"));
				if(empty($findSameEmail) || (isset($findSameEmail["source.toBeValidated"]) && $findSameEmail["source.toBeValidated"]==true)){
					$orga["source"]["toBeValidated"]=true;
				}
				// CREATE COLLECTIF LOCAL
				$orga = Element::prepData($orga) ;
                PHDB::insert(Organization::COLLECTION, $orga );

				$slug=Slug::checkAndCreateSlug($orga["name"]);
    			Slug::save(Organization::COLLECTION,(String)$orga["_id"],$slug);
    			PHDB::update(Organization::COLLECTION,
							array("_id" => (String)$orga["_id"]) , 
							array('$set' => array("slug" => $slug)));
    			
    			// Cas ou un groupe est créé mais le code postal est commun donc mail commun
				if(!empty($findSameEmail)){
					if(!isset($findSameEmail["source.toBeValidated"]) || empty($findSameEmail["source.toBeValidated"])){
						// TODO MAIL TO FRAMA
						$sub=explode("@", $findSameEmail["email"])[0];
						$paramsMails = array("tplMail" => $sub."-subscribe@listes.transition-citoyenne.org",
											"tplObject" => "sub ".$sub,
											"tpl" => "basic",
											"fromMail"=>$userMail,
											"html" => $html);
						Mail::createAndSend($paramsMails);
						if(isset($_POST["msgGroup"])){
								$tplMsgList='<span><b>'.$_POST["name"].'</b> a écrit ce message :</span><br/><span style="padding:15px; margin-top:10px;background-color:#f9f9f9; border:1px solid #eee; border-radius: 10px;float:left;">'.$_POST["msgGroup"].'</span>';
						
								$paramsMails = array("tplMail" => $findSameEmail["email"],
									"tplObject" => "Message d'un nouveau membre sur le ".$sub,
									"tpl" => "basic",
									"fromMail"=>$userMail,
									"html" => $tplMsgList);

							Mail::createAndSend($paramsMails);
						}
					}else{
						$sendTopacteCreate=true;
					}
				}else
					$sendTopacteCreate=true;
			}
			$res["result"] = true;

			if( $sendTopacteCreate===true) {
				$html = "<span>Une nouvelle personne a crée le collectif : ".$name."</span><br/><span>Mail de la liste à créer : ".$res["email"]."</span><br/><span>Mail de l'utilisateur : ".$_POST["email"]."</span><br/>";
    				$paramsMails = array("tplMail" => Yii::app()->session["costum"]["admin"]["email"],
									"tplObject" => "Un nouveau membre ".$_POST["name"],
									"tpl" => "basic",
									"html" => $html);
				Mail::createAndSend($paramsMails);
			}

			//Inscription newsletter pacte
			$paramsMails = array("tplMail" => "collectifs-locaux-subscribe@listes.transition-citoyenne.org",
							"tplObject" => "sub ".$_POST["email"],
							"tpl" => "basic",
							"fromMail"=>$_POST["email"],
							"html" => "");
			Mail::createAndSend($paramsMails);
			
			/*$ch = curl_init();
			//$curl = curl_init();

			curl_setopt_array($ch, array(
				CURLOPT_HTTPHEADER => array('Accept: application/json', 'Content-Type: application/json', 'api-key: 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'),
			  	CURLOPT_URL => "https://api.sendinblue.com/v3/contacts",
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "POST",
			  	CURLOPT_POSTFIELDS => "{\"email\":\"".$_POST["email"]."\"}"//json_encode(array("email" => $_POST["email"]))
			));
		    $resultat = curl_exec($ch);
		    $err = curl_error($ch);
			curl_close($ch);
		    if ($err) {
		    	$res = array(
					"result" => false,
					"msg"  => "Un problème est survenu lors de votre inscription à la mailing du pacte:".$msg." / Contacter l'admin"
				);
		        //print curl_error($ch);
		    }*/
			
		}

		
		Rest::json($res);
	}
}