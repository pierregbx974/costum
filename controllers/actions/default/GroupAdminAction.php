<?php 
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class GroupAdminAction extends CAction
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();

		if(!empty(Yii::app()->session["costum"]["slug"]) && !empty(Yii::app()->session["costum"]["htmlConstruct"]["adminPanel"]["groupe"])){

			$panelAdmin = Yii::app()->session["costum"]["htmlConstruct"]["adminPanel"]["groupe"] ;
			$limitMin=0;
			$stepLim=100;
			if(@$_POST["page"]){
				$limitMin=$limitMin+(100*$_POST["page"]);
			}

			$search="";
			if(@$_POST["text"] && !empty($_POST["text"])){
				$search = trim(urldecode($_POST['text']));
			}
			// if(	isset(Yii::app()->session["costum"]) 
			// 	&& isset(Yii::app()->session["costum"]["slug"]))
			// 	$sourceKey=Yii::app()->session["costum"]["slug"];
			$query = array();
			$query = Search::searchString($search, $query); 
			$query = Search::searchSourceKey(Yii::app()->session["costum"]["slug"], $query);

			//var_dump($query);
			$params["typeDirectory"]=(!empty($panelAdmin["types"]) ? $panelAdmin["types"] : [Organization::COLLECTION]);
			$params["results"] = array();
			if(!empty($panelAdmin["types"])){
				foreach ($panelAdmin["types"] as $key => $value) {
					$params["results"][$value] = PHDB::findAndLimitAndIndex ( $value , $query, $stepLim, $limitMin);

					if($tpl!="json")
						$params["results"]["count"][$value] = PHDB::count( $value , $query);
				}
			}
			
			//var_dump($params); exit;
			
			$page = "groupAdmin";
			if($tpl=="json")
				Rest::json( $params );
			else if(Yii::app()->request->isAjaxRequest)
				echo $controller->renderPartial("/custom/default/".$page,$params,true);
		}else{
			
		}
		
		
	}
}
